<style>
img {
  display: block;
  margin-left: auto;
  margin-right: auto;
  width: 70%;
}
h1 {
	text-align:center;
}
body {
text-align: justify;
}
</style> 
# 3D Milling 

For the task of this week the goal was to create a 3D model and then mill it. The model should connect two mechanical components. This task is similar to the one we did last week, except that we now use subtrative manufacturing.  

To complete the task, we could use either aluminum or POM.

## Z Axis Attachment

To complete the assignment, I decided to mill a 3D part of my Z-axis of the CNC machine I am creating. This piece is attached to the linear guides of the Y-axis and holds the linear rods of the Z-axis. 

![](../images/week06/6.png)

Since it is not possible to mill all the holes provided on the part, I decided to mill only the holes for the linear guide. In this way, the less long side of the object would be the Z-axes of the stock, which would reduce the movement of the workpiece during milling and also allow the use of a less long tool to avoid collisions with the stock.
  
![](../images/week06/3.png)

![](../images/week06/4.png)

## Using the Fusion 360 CAM Processor

To switch to the CAM Processor, you need to change the environment and select the <strong>Manufacture</strong> option. Then you have to create a new setup to generate the stock. 

To continue now, you will need a piece of POM into which the object to be produced will fit without wasting a lot of material during milling. The dimensions of my 3D model are 50x74x40mm. I found a piece of POM with the sizes 60.3x85x49.1mm and decided to use it.  

![](../images/week06/5.png)

Next, you must select the zero point. It is important to specify the correct axes, which correspond to the block placed on the CNC machine, as well as the tool origin point. In this case, it is the upper left-hand corner.

![](../images/week06/7.png)

Now you can start to create the stock. Enter the correct dimension of the workpiece based on your measurement, if you are not sure, always make it slightly smaller and not larger. 

![](../images/week06/8.png)

Once the new configuration is created, we can start creating the toolpath. To do this, you normally always start with a clearance job, such as the adaptive cut. Go to 3D -> Adaptive Cut to initiate the process.

As you can see, the starting point of our tool is the selected box point. First we select a suitable tool for this machining operation and then we configure the feed and speed.  

For milling POM with a 6 mm end milling cutter with two flutes I used the following configurations: 

* <b>Spindel speed:			7000 rpm
* Cutting Feedrate:			300 mm/min
* Lead-In/Out-Feedrate:		1000 mm/min	</b>


![](../images/week06/9.png)

When you create a new tool, always measure the flute length and body length very accurately, as this is very critical.

![](../images/week06/10.png)

It is also very important to select the correct tool number under the Post Process tab, as the CAM Processor will put the milling tool back after the milling job is finished. The machine will crash, if in this case another tool is already installed under this number.   

![](../images/week06/11.png)

Then switch to the geometry and make sure that the box <b>Rest Machining</b> is not selected because there was no previous milling operation.

![](../images/week06/12.png)

A very critical point is the height. Here you must ensure that the bottom height is slightly lower than the actual height of the material or model to ensure that the part is milled to the end.

![](../images/week06/13.png)

Finally, go to the Passes tab and enter the configurations for the optimal load and stepdown. Normally, the maximum load is half the tool size. In this case it would be 3 mm. The tolerance indicates how detailed the result will be. Next, make sure that the stock to leave box is not selected, because we do not plan to perform a smooth operation after this operation. 

![](../images/week06/14.png)

Now generate the toolpath and check it for collisions. Select the Adaptive machine job and go to Actions -> Simulate. 

![](../images/week06/15.png)

![](../images/week06/16.png)

I created two other operations contouring and drilling to make the holes for the linear guides.

Next, select Setup-> Post Process to generate the G-Code. Save the G-Code on a USB stick and start the milling job. 

![](../images/week06/17.png)

To fix the POM piece I used the vacuum and the double-sided adhesive tape. The height of the stock was chosen as Z-point. Below you can see the result, which looks really good. I have to drill the remaining holes manually, using masks that I create with the Laser Cutter. 

![](../images/week06/1.jpeg)

![](../images/week06/2.jpeg)

### Download 

[Download the Z-axis attachment of my CNC Machine]("../files/week06/Z-Axis_Attachment.stl.zip")
 
## Useful Links

* [Fusion 360]("https://www.autodesk.de/products/fusion-360/students-teachers-educators?&mkwid=s%7Cpcrid%7C374688844453%7Cpkw%7C%7Cpmt%7Cb%7Cpdv%7Cc%7Cslid%7C%7Cpgrid%7C76443707453%7Cptaid%7Cdsa-632887289686%7C&intent=&utm_medium=cpc&utm_source=google&utm_campaign=&utm_term=&utm_content=s%7Cpcrid%7C374688844453%7Cpkw%7C%7Cpmt%7Cb%7Cpdv%7Cc%7Cslid%7C%7Cpgrid%7C76443707453%7Cptaid%7Cdsa-632887289686%7C&gclid=EAIaIQobChMIgrvch_vl5gIVleh3Ch2ZyA_JEAAYASAAEgK_HvD_BwE&gclsrc=aw.ds")
* [Fabacademy Alessandra Crotty - Molding and Casting](http://fabacademy.org/2019/labs/kamplintfort/students/alessandra-crotty/week9.html#fünf)

