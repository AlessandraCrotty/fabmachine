
<style>
img {
  display: block;
  margin-left: auto;
  margin-right: auto;
  width: 60%;
}
h1 {
	text-align:center;
}
body {
text-align: justify;
}
</style> 
# About Me
![](../images/aboutme/7.jpg)

I am Alessandra Crotty and you are visiting my machine class website. Currently I am studying towards a bachelor degree in Media Communication Computer Science at the University Rhein-Waal. 

From the moment I entered the Fab Lab, I fell in love with digital manufacturing. I am a graduate of the Fab Academy course 2019. 
 
I like to travel and cook. Nothing beats a delicious meal! I also like to explore and try out new things. 

## My background

I was born in Fiesole. We lived in Italy until my 6th birthday and then we moved to the Netherlands. When I was 13, we moved to Germany and I've been living here ever since. 

## Previous work

During the Fab Academy course I produced several printed circuit boards. As a final project I made a go-kart in cooperation with another Fab Academy student. The frame of the go-kart is made out of wood and epoxy resin. It's weight is about 40 kg including the car battery. It also has automatic steering, which is converted by a potentiometer, that changes the number of revolutions of the engines.  

![](../images/aboutme/kart.jpg)

If you would like to learn more about our final project, you can visit my Fab Academy website [Fab Academy website](http://fabacademy.org/2019/labs/kamplintfort/students/alessandra-crotty/project.html). Each assignment including the final project is open source and well documented.